package receiver;

/**
 * @(#) EditorEngine.java
 */

public interface EditorEngine {
	public void copy();
	
	public void paste();
	
	public void cut();
	
	public void erase();
	
	public void type();	
}
